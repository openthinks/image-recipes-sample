image: $DOCKER_IMAGE

######
# variables
#
# The main variables to customize the built artifacts

variables:
  osname: apertis
  release: "v2021"
  mirror: https://repositories.apertis.org/apertis/

default:
  interruptible: true
  retry:
    max: 1
    when:
      - job_execution_timeout
      - script_failure

stages:
  - build-env
  - preparation
  - ospack build
  - apt image build

######
# Permissions fixup
#
# GitLab uses a too-lax umask when checking out the repositories, which leads
# to root-owned  world-writable files being put in the images.
#
# The image-builder container defaults to umask 022 which would only affects
# the w bit, so reset it for group and other users.
#
# See https://gitlab.com/gitlab-org/gitlab-runner/issues/1736
before_script: &gitlab_permissions_fixup
  - chmod -R og-w .
  - chmod -R a-w overlays/sudo-fqdn

######
# templates
#
# The files included here contain the actual definitions of the jobs as templates
# that need to be instantiated with `extends` while setting the variables that
# control which ospack/hwpack to use.

include:
  - /.gitlab-ci/templates-ospack-build.yml
  - /.gitlab-ci/templates-artifacts-build.yml

.lightweight:
  tags:
    - lightweight

######
# ospacks
#
# These variables are meant to be used when instantiating the templates
# included above to choose the desired ospack flavours

.rpi64_containers:
  variables:
    type: rpi64_containers

######
# hwpacks
#
# Another set of variables meant to be used when instantiating the templates
# included above, choosing the hwpack flavors

.arm64-uboot:
  variables:
    architecture: arm64
    board: uboot

.arm64-rpi64:
  variables:
    architecture: arm64
    board: rpi64

######
# stage: preparation

debos-dry-run:
  stage: preparation
  extends:
    - .lightweight
  script:
    - 'for i in *.yaml ; do debos --print-recipe --dry-run "$i" ; done'

prepare-build-env:
  stage: build-env
  extends:
    - .lightweight
  image: debian:bullseye-slim
  before_script:
    - apt update && apt install -y --no-install-recommends
        ca-certificates
        curl
        jq
        skopeo
  script:
    # multiline variables have CRLF line endings, likely because
    # x-www-form-urlencoded and form-data mandate them, so let's get rid of the
    # extra `\r` and ensure there's a line terminator at the end of the file
    # (by appending nothing, but it works)
    - test -n "$BUILD_ENV_OVERRIDE" && sed -i 's/\r$// ; $a\'  "$BUILD_ENV_OVERRIDE" && . "$BUILD_ENV_OVERRIDE"
    - PIPELINE_VERSION=${PIPELINE_VERSION:-$(date +%Y%m%d.%H%M)}
    - DOCKER_IMAGE_BASE=$CI_REGISTRY/infrastructure/${osname}-docker-images/${release}-image-builder
    - DOCKER_IMAGE=${DOCKER_IMAGE:-$DOCKER_IMAGE_BASE@$(skopeo --creds "$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD" inspect docker://$DOCKER_IMAGE_BASE | jq -r .Digest)}
    - mkdir -p a/$PIPELINE_VERSION/meta
    - BUILD_ENV=a/$PIPELINE_VERSION/meta/build-env-$CI_PROJECT_NAME.txt
    - echo "PIPELINE_VERSION=$PIPELINE_VERSION" | tee -a $BUILD_ENV
    - echo "DOCKER_IMAGE=$DOCKER_IMAGE" | tee -a $BUILD_ENV
    - echo "RECIPES_COMMIT=$CI_COMMIT_SHA" | tee -a $BUILD_ENV
    - echo "RECIPES_URL=$CI_PROJECT_URL"  | tee -a $BUILD_ENV
    - echo "PIPELINE_URL=$CI_PIPELINE_URL" | tee -a $BUILD_ENV
    - APT_SNAPSHOT_LATEST_URL="${mirror%/}/dists/${release}/snapshots/latest.txt"
    - echo "Fetching $APT_SNAPSHOT_LATEST_URL"
    - APT_SNAPSHOT_LATEST=$(curl --fail --silent --show-error "$APT_SNAPSHOT_LATEST_URL")
    - APT_SNAPSHOT=${APT_SNAPSHOT:-$APT_SNAPSHOT_LATEST}
    - echo "APT_SNAPSHOT=$APT_SNAPSHOT" | tee -a $BUILD_ENV
    - test -n "$BUILD_ENV_OVERRIDE" && diff --color=always -U 10 "$BUILD_ENV_OVERRIDE" "$BUILD_ENV" || true
  artifacts:
    expire_in: 1d
    paths:
      - a/*/meta/build-env-$CI_PROJECT_NAME.txt
    reports:
      dotenv: a/*/meta/build-env-$CI_PROJECT_NAME.txt

######
# stage: ospack build

ospack-build-arm64-rpi64_containers:
  extends:
    - .ospack-build
    - .arm64-uboot
    - .rpi64_containers

######
# stage: artifacts build

apt-image-build-arm64-rpi64_containers-rpi64:
  extends:
    - .apt-image-build
    - .arm64-rpi64
    - .rpi64_containers
  needs:
    - prepare-build-env
    - ospack-build-arm64-rpi64_containers
